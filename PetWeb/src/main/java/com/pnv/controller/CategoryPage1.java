package com.pnv.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class CategoryPage1 {
	@RequestMapping(value="/manager_category", method = RequestMethod.GET )
	public String viewCategoryPage(ModelMap map){
		return "admin/manager_category";
	}
}

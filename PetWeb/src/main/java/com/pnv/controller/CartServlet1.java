package com.pnv.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.pnv.business.ProductBO;
import com.pnv.model.Cart;
import com.pnv.model.Item;
import com.pnv.model.Product;

@Controller
public class CartServlet1 {
	
	ProductBO productBO;
	@RequestMapping(value="/CartServlet", method=RequestMethod.POST)
	
	public void viewCartPage(ModelMap map, HttpServletRequest request, HttpServletResponse response) throws IOException{
		HttpSession session = request.getSession();
		String command = request.getParameter("command");
		String productID = request.getParameter("productID");
		Cart cart = (Cart) session.getAttribute("cart");

		try {
			Long idProduct = Long.parseLong(productID);
			Product product = productBO.getProduct(idProduct);
			switch (command) {
			case "plus":
				if (cart.getCartItems().containsKey(idProduct)) {
					cart.plusToCart(idProduct, new Item(product, cart.getCartItems().get(idProduct).getQuantity()));
				} else {
					cart.plusToCart(idProduct, new Item(product, 1));
				}
				break;
			case "remove":
				cart.removeToCart(idProduct);
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.sendRedirect("/petShop");
		}
		
		session.setAttribute("cart", cart);
		response.sendRedirect("/petShop");
	}

}

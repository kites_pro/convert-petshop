package com.pnv.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.pnv.dao.CategoryDAO;
import com.pnv.model.Category;

@Controller
public class ManagerCategoryServlet1 {
	CategoryDAO categoryDAO;

	@RequestMapping(value = "/manager_categoryServlet", method = RequestMethod.GET)
	public String viewManagerCategoryPage(ModelMap map, HttpServletRequest request) {
		String command = request.getParameter("command");
		String category_id = request.getParameter("category_id");

		String url = "";
		try {
			switch (command) {

			case "delete":
				categoryDAO.deleteCategory(Long.parseLong(category_id));
				url = "admin/manager_category";
				break;
			}
		} catch (Exception e) {
		}
		return url;
	}
	
	@RequestMapping(value="/manager_categoryServlet", method = RequestMethod.POST)
	public String viewCategoryPage(ModelMap map, HttpServletRequest request){
		 String command = request.getParameter("command");
	        String tenDanhMuc = request.getParameter("tenDanhMuc");

	        String url = "", error = "";
	        if (tenDanhMuc.equals("")) {
	            error = "Vui lòng nhập tên danh mục!";
	            request.setAttribute("error", error);
	        }

	        try {
	            if (error.length() == 0) {
	                switch (command) {
	                    case "insert":
	                        categoryDAO.insertCategory(new Category(new Date().getTime(), tenDanhMuc));
	                        url = "admin/manager_category";
	                        break;
	                    case "update":
	                        categoryDAO.updateCategory(new Category(Long.parseLong(request.getParameter("category_id")),
	                               tenDanhMuc));
	                        url = "admin/manager_category";
	                        break;
	                }
	            } else {
	                url = "admin/insert_category";
	            }
	        } catch (Exception e) {
	        }
	        return url;
	}
}

package com.pnv.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ContactPage1 {
	@RequestMapping(value="/contact", method=RequestMethod.GET)
	public String viewContactPage(ModelMap map){
		return "users/contact";
	}
}

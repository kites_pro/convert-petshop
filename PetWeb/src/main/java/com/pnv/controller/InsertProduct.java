package com.pnv.controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.pnv.business.AddProductBO;
import com.pnv.business.AddProductImplBO;
import com.pnv.model.Product;
/**
 * Servlet implementation class InsertProduct
 */
//@WebServlet("/InsertProduct")
public class InsertProduct extends HttpServlet {
	
	private final AddProductBO addProductBO = new AddProductImplBO();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InsertProduct() {
        super();
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		RequestDispatcher view = request.getRequestDispatcher("jsp/admin/insert_product.jsp");
		view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Long ID= Long.valueOf("product_id");
		Long categery = Long.valueOf("catogery_id");
		String name = request.getParameter("product_name");
        String image= request.getParameter("product_image");
        Double price = Double.parseDouble(request.getParameter("product_price"));
        int prices = (int) Math.round(price);
        String description= request.getParameter("product_description");
		HttpSession session = request.getSession();	
//		Product product = new Product(ID, categery, name, image, prices, description);
//		request.setAttribute("product", product);
//		request.getRequestDispatcher("manager_product.jsp").forward(request, response);
		try {
			Product p = new Product();
			p.setProductID(ID);
			p.setCategoryID(categery);
			p.setProductName(name);
			p.setProductImage(image);
			p.setProductPrice(prices);
			p.setProductDescription(description);
			addProductBO.addProduct(p);
			
//		    request.setAttribute("product", p);
			Product product = new Product(ID, categery, name, image, prices, description);
		} catch (Exception e) {
			
		}

		response.sendRedirect("/admin/");
	}

}

package com.pnv.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.pnv.business.UsersImplBO;
import com.pnv.model.Users;
import com.pnv.util.MD5;

@Controller
public class UsersServlet1 {
	
	UsersImplBO usersBO = new UsersImplBO();
	
	@RequestMapping(value="/users",  method=RequestMethod.POST)
	
	public String viewUsersPage(ModelMap map, HttpServletRequest request, HttpServletResponse response) throws IOException{
		
		HttpSession session = request.getSession();
		String command = request.getParameter("command");
		String url = "";
		Users users = new Users();
		
		switch (command) {
		case "insert":
			users.setUserID(new java.util.Date().getTime());
			String user = request.getParameter("email");
			users.setUserEmail(user);
			String pass = request.getParameter("pass");
			users.setUserRole(true);
			if (pass.equals("") || user.equals("") || usersBO.checkEmail(request.getParameter("email"))) {
				url = "users/register";

			} else {
				String pw = MD5.encryption(pass);
				users.setUserPass(pw);
				usersBO.insertUser(users);
				session.setAttribute("user", users);
				response.sendRedirect("/");
			}
			break;

		case "login":
			users = usersBO.login(request.getParameter("email"), MD5.encryption(request.getParameter("pass")));
			if (users != null && users.isUserRole() == true) {
				session.setAttribute("user", users);
				url = "/";
			} else if(users != null && users.isUserRole() == false){
				session.setAttribute("user", users);
				url = "/admin";
			} else {
				session.setAttribute("error", "Error email or password!");
				url = "users/login";
			}
			break;
		}
		return url;
	}
}

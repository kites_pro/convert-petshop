package com.pnv.controller;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.pnv.business.ProductBO;
import com.pnv.business.ProductImplBO;
import com.pnv.model.Cart;
import com.pnv.model.Product;

@Controller
public class ProductPage1 {
	@RequestMapping(value="/product",  method=RequestMethod.GET)
	public String viewProductPage(ModelMap map, HttpServletRequest request){
		ProductBO productBO = new ProductImplBO();
		HttpSession session = request.getSession();
		long categoryID = 0;
		if (request.getParameter("categoryID") != null) {
			categoryID = (long) Long.parseLong(request.getParameter("categoryID"));
		}
		Cart cart = (Cart) session.getAttribute("cart");
		if (cart == null) {
			cart = new Cart();
			session.setAttribute("cart", cart);
		}
		
		int pages = 0, firstResult = 0, maxResult = 0, total = 0, maxPage=0;
		if (request.getParameter("pages") != null) {
			pages = (int) Integer.parseInt(request.getParameter("pages"));
		}

		try {
			total = productBO.countProductByCategory(categoryID);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (total <= 8) {
			firstResult = 0;
			maxResult = total;
		} else {
			firstResult = (pages - 1) * 8;
			maxResult = 8;
		}
		maxPage = (int) (total/8) +1;
		
		try {
			ArrayList<Product> listProduct = productBO.showListProductWithNav(categoryID, firstResult, maxResult);
			request.setAttribute("product", listProduct);
			request.setAttribute("total", total);
			request.setAttribute("categoryID", categoryID);
			request.setAttribute("currentPage", pages);
			request.setAttribute("maxPage", maxPage);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "users/product";
	}
}

package com.pnv.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.pnv.business.CategoryBO;
import com.pnv.business.CategoryImplBO;

@Controller
public class SinglePage1 {

	@RequestMapping(value = "/single", method = RequestMethod.GET)
	
	public String viewSinglePage(ModelMap map, HttpServletRequest request){
		HttpSession session = request.getSession();
		CategoryBO categoryBO = (CategoryImplBO) session.getAttribute("");
		return "users/single";
	}
}

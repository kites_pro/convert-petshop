package com.pnv.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class LogAdmin {
@RequestMapping(value = "/login_admin", method = RequestMethod.GET)
	
	public String viewAdminPage(ModelMap map){
		return "admin/login";
	}
}

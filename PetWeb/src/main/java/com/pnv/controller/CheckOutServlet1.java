package com.pnv.controller;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.pnv.business.BillBO;
import com.pnv.business.BillDetailBO;
import com.pnv.model.Bill;
import com.pnv.model.BillDetail;
import com.pnv.model.Cart;
import com.pnv.model.Item;
import com.pnv.model.Users;
import com.pnv.util.SendMail;

@Controller
public class CheckOutServlet1 {
	private BillBO billBO;
	private BillDetailBO billDetailBO;
	
	@RequestMapping(value="/checkOut", method=RequestMethod.GET)
	public String viewCheckOutPage(ModelMap map){
		return "users/checkout";
	}
	
	@RequestMapping(value="/checkOut", method=RequestMethod.POST)
	public void checkOut(ModelMap map, HttpServletRequest request, HttpServletResponse response) throws IOException{
		String payment = request.getParameter("payment");
		String address = request.getParameter("address");
		HttpSession session = request.getSession();
		Cart cart = (Cart) session.getAttribute("cart");
		Users users = (Users) session.getAttribute("user");
	
		try {
			long ID = new Date().getTime();
			Bill bill = new Bill();
			bill.setBillID(ID);
			bill.setAddress(address);
			bill.setPayment(payment);
			bill.setUserID(users.getUserID());
			bill.setDate(new Timestamp(new Date().getTime()));
			bill.setTotal(cart.totalCart());
			billBO.addBill(bill);
			for (Map.Entry<Long, Item> list : cart.getCartItems().entrySet()) {
				billDetailBO.addBillDetail(new BillDetail(0, ID, list.getValue().getProduct().getProductID(),
						list.getValue().getProduct().getPrice(), list.getValue().getQuantity()));
			}
			new SendMail().sendMail(users.getUserEmail(), "Pets Shop",
					"Hello, " + users.getUserEmail() + "\nTotal: " + cart.totalCart());
			cart = new Cart();
			session.setAttribute("cart", cart);
		} catch (Exception e) {
		}
		response.sendRedirect("/PetWeb/");
	}
}

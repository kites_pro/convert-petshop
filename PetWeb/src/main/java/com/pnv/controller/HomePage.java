package com.pnv.controller;

import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.pnv.business.ProductBO;
import com.pnv.business.ProductImplBO;
import com.pnv.model.Cart;
import com.pnv.model.Product;

@Controller
public class HomePage {
		@RequestMapping(value = "/", method = RequestMethod.GET)
		
		public String viewHomePage(HttpServletRequest request , ModelMap map){
			ProductBO productBO = new ProductImplBO();
			HttpSession session = request.getSession();
			Cart cart = (Cart) session.getAttribute("cart");
			if (cart == null) {
				cart = new Cart();
				session.setAttribute("cart", cart);
			}

			int page = 1;
			int recordsPerPage = 8;
			if (request.getParameter("pages") != null)
				page = Integer.parseInt(request.getParameter("pages"));

			
			int noOfRecords = 0;
			try {
				noOfRecords = (int) productBO.countAllProducts();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			int noOfPages = 0;
			if ((noOfRecords % recordsPerPage) == 0){
				noOfPages = noOfRecords/recordsPerPage ; 
			}
			else{
				noOfPages = (noOfRecords/recordsPerPage)+1;
			}
			

			try {
				List<Product> listProduct = productBO.showAllProducts((page - 1) * recordsPerPage, recordsPerPage);
				request.setAttribute("product", listProduct);
				request.setAttribute("noOfPages", noOfPages);
				request.setAttribute("currentPage", page);
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return "users/index";
		}
	
}

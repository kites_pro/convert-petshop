package com.pnv.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class BillPage {
	@RequestMapping(value = "/manager_bill", method = RequestMethod.GET)
	
	public String viewBillPage(ModelMap map){
		return "admin/manage_bill";
	}
	
}

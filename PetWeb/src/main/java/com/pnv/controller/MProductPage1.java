package com.pnv.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class MProductPage1 {
	@RequestMapping(value="/manager_product", method = RequestMethod.GET )
	public String viewManagerProduct(ModelMap map){
		return "admin/manager_product";
	}
}

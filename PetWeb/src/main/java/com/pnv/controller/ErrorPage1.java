package com.pnv.controller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ErrorPage1 {
	@RequestMapping(value="/error", method=RequestMethod.GET)
	public String viewErrorPage(ModelMap map){
		return "users/error";
	}
}

package com.pnv.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.pnv.model.Users;

@Controller
public class LoginPage1 {
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	
	public void viewBillPage(ModelMap map, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		HttpSession session = request.getSession();
		session.setAttribute("error", "");
		
		Users users = (Users) session.getAttribute("user");
		if (users == null) {
			request.getRequestDispatcher("WEB-INF/jsp/users/login.jsp").forward(request, response);
		} else {
			request.getRequestDispatcher("").forward(request, response);
		}
	}
}

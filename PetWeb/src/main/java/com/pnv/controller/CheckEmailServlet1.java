package com.pnv.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.pnv.business.UsersBO;
import com.pnv.business.UsersImplBO;

@Controller
public class CheckEmailServlet1 {
	UsersBO usersBO = new UsersImplBO();

	@RequestMapping(value="/CheckEmailServlet",  method=RequestMethod.GET)
	public void checkEmail (ModelMap map, HttpServletRequest request, HttpServletResponse response) throws IOException{		
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");	
	}
	
	@RequestMapping(value="/CheckEmailServlet",  method=RequestMethod.POST)
	public void doCheckEmailServlet (ModelMap map, HttpServletRequest request, HttpServletResponse response) throws IOException{	
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		if (usersBO.checkEmail(request.getParameter("username"))) {
			response.getWriter().write("<img src=\"img/not-available.png\" />" + "<b> Email is Existed");
		} else {
			response.getWriter().write("<img src=\"img/available.png\" />" + "<b> Can use");
		}
	}
}

package com.pnv.dao;

import java.awt.Image;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import com.pnv.model.Product;
import javax.swing.ImageIcon;
import com.pnv.util.DBConnect;

public class SearchImpl implements Search {
	private PreparedStatement ps;
	private String sql = null;
	private ResultSet rs;
	
	public List<Product> getAllBySearchProduct() {
		Connection connection = DBConnect.getConnection();
		List<Product> list = new ArrayList<Product>();
		try {
			sql = "SELECT product_id, product_name, product_image, product_price, product_description FROM product";
			ps = connection.prepareStatement(sql);
			rs = ps.executeQuery();

			Product sp;
			while (rs.next()) {
				sp = new Product();
				sp.setProductID(rs.getLong("product_id"));
				sp.setProductName(rs.getString("product_name"));
				sp.setProductPrice(rs.getInt("product_price"));
				sp.setProductImage(rs.getString("product_image"));
				sp.setProductDescription(rs.getString("product_description"));
				list.add(sp);
			}
		} catch (Exception e) {
			System.out.println("Sorry!!!!");
		}
		try {
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	@Override
	public List<Product> getSearchByCondition(String condition) {
		Connection connection = DBConnect.getConnection();
		List<Product> devices = new ArrayList<Product>();
		try {
			sql = "SELECT product_id, product_name, product_image, product_price, product_description FROM product WHERE product_name like ?";
			ps = connection.prepareStatement(sql);
			ps.setString(1, "%" + condition + "%");
			rs = ps.executeQuery();
			Product sp;
			while (rs.next()) {
				sp = new Product();
				sp.setProductID(rs.getLong("product_id"));
				sp.setProductName(rs.getString("product_name"));
				sp.setProductPrice(rs.getInt("product_price"));
				sp.setProductImage(rs.getString("product_image"));
				sp.setProductDescription(rs.getString("product_description"));
				devices.add(sp);
			}
		} catch (Exception e) {
			System.out.println("Sorry!!!!");
		}
		try {
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return devices;
	}
	
	public List<Product> getSearchByRangeOfPrice(float price1, float price2){
		Connection connection = DBConnect.getConnection();
		List<Product> devices = new ArrayList<Product>();
		try {
			sql = "SELECT product_id, product_name, product_image, product_price, product_description FROM product WHERE product_price BETWEEN ? AND ?";
			ps = connection.prepareStatement(sql);
			ps.setFloat(1, price1);
			ps.setFloat(2, price2);
			rs = ps.executeQuery();
			Product sp;
			while (rs.next()) {
				sp = new Product();
				sp.setProductID(rs.getLong("product_id"));
				sp.setProductName(rs.getString("product_name"));
				sp.setProductPrice(rs.getInt("product_price"));
				sp.setProductImage(rs.getString("product_image"));
				sp.setProductDescription(rs.getString("product_description"));
				devices.add(sp);
			}
		} catch (Exception e) {
			System.out.println("Sorry!!!!");
		}
		try {
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return devices;
	}
	
	public static void main(String[] args) {
		
		new SearchImpl().getAllBySearchProduct();
		
	}

}

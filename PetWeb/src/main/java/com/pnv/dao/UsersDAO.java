package com.pnv.dao;

import com.pnv.model.Users;

public interface UsersDAO {
	public boolean checkEmail(String email);
	
	public boolean insertUser(Users u);
	
	public Users login(String email, String password);
	
	public Users getUser(long userID);
	
	public Users adminLogin(String email, String password, boolean userRole);
	
	
}

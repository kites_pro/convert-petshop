package com.pnv.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import com.pnv.model.Product;
import com.pnv.util.DBConnect;

public class InsertDAO {
	public void insertProduct(Product p) throws SQLException {
        Connection connection = DBConnect.getConnection();
        String sql = "INSERT INTO product VALUES(?,?,?,?,?,?)";
        PreparedStatement ps = connection.prepareCall(sql);
        ps.setLong(1, p.getProductID());
        ps.setLong(2, p.getCategoryID());
        ps.setString(3, p.getProductName());
        ps.setString(4, p.getProductImage());
        ps.setDouble(5, p.getPrice());
        ps.setString(6, p.getProductDescription());
        ps.executeUpdate();
    }
	public static void main(String[] args) throws SQLException {
		new InsertDAO().insertProduct(new Product());
	}
}

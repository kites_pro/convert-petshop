package com.pnv.dao;

import java.util.List;
import com.pnv.model.Product;


public interface Search {
	List<Product> getAllBySearchProduct();
	List<Product> getSearchByCondition(String condition);
	List<Product> getSearchByRangeOfPrice(float price1, float price2);
}

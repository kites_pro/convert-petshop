package com.pnv.business;
import com.pnv.model.Product;

public interface AddProductBO {
	
	public void addProduct(Product p);
	
}

package com.pnv.business;

import java.util.List;

import com.pnv.dao.Search;
import com.pnv.dao.SearchImpl;
import com.pnv.model.Product;

public class SearchImplBO implements SearchBO{

	Search search = null;
	
	public SearchImplBO() {
		this.search = new SearchImpl();
	}
	@Override
	public List<Product> showAllProductWithSearch() {
		return this.search.getAllBySearchProduct();
	}

	@Override
	public List<Product> showProductsWithSearchCondition(String condition) {
		return this.search.getSearchByCondition(condition);
	}

	@Override
	public List<Product> showProductInRangeOfPrice(float price1, float price2) {
		return this.search.getSearchByRangeOfPrice(price1, price2);
	}
	
}

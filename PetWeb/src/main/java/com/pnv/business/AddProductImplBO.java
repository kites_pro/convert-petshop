package com.pnv.business;

import java.sql.SQLException;

import com.pnv.dao.InsertDAO;
import com.pnv.dao.ProductDAO;
import com.pnv.model.Product;

public class AddProductImplBO implements AddProductBO{
	InsertDAO productDAO= null;
	public AddProductImplBO(){
		this.productDAO= new InsertDAO();
	}
	@Override
	public void addProduct(Product p) {
		try {
			this.productDAO.insertProduct(p);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
}

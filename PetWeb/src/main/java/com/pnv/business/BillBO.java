package com.pnv.business;

import java.util.ArrayList;

import com.pnv.model.Bill;

public interface BillBO {
	
	public void addBill(Bill bill);
	
	public ArrayList<Bill> showListBill();
	
	
}

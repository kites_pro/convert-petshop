package cop;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mysql.jdbc.StringUtils;
import com.pnv.business.ProductBo;
import com.pnv.business.SearchBO;
import com.pnv.business.SearchBOImpl;
import com.pnv.dao.ProductDao;
import com.pnv.models.Product;

@Controller
public class Search {
	
	@Autowired
	private SearchBO searchBO;
	@Autowired
	private ProductBo prod;
	
	
	private String value1 = "Under 100.000";
	private String value2 = "100.000-300.000";
	private String value3 = "300.000-500.000";
	private String value4 = "More 500.000";
	private int price1 = 0;
	private int price2 = 100000;
	private int price3 = 300000;
	private int price4 = 500000;
	private int price5 = 10000000;
	
	
	@RequestMapping(value = "/side", method = RequestMethod.GET)
	public String viewSearchProduct(ModelMap map, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		response.setContentType("text/html");
		request.setCharacterEncoding("utf-8");
		String conditionSearch = request.getParameter("Price");

		List<Product> products = null;

		if (conditionSearch.equals(value1)) {
			products = searchBO.showProductInRangeOfPrice(price1, price2);
		} else if (conditionSearch.equals(value2)) {
			products = searchBO.showProductInRangeOfPrice(price2, price3);
		} else if (conditionSearch.equals(value3)) {
			products = searchBO.showProductInRangeOfPrice(price3, price4);
		} else if (conditionSearch.equals(value4)) {
			products = searchBO.showProductInRangeOfPrice(price4, price5);
		}
		request.setAttribute("products", products);

		return "users/result";
	}
	
	@RequestMapping(value = "/side", method = RequestMethod.POST)
	public String doSearchProduct(ModelMap map, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		response.setContentType("text/html");
		request.setCharacterEncoding("utf-8");
		
		
		String conditionSearch = request.getParameter("Search");
		if (StringUtils.isNullOrEmpty(conditionSearch)) {
			request.setAttribute("MS", "Sorry! You don't input data.");
			return "users/message";
		} else if (prod.checkSearch(conditionSearch)) {
			List<Product> products= searchBO.showProductsWithSearchCondition(conditionSearch);
			request.setAttribute("products", products);

			return "users/result";
		} else {
			request.setAttribute("MS", "Sorry! Product not found " + "'" + conditionSearch + "'");

			return "users/message";
		}
	}
}

package cop;

import java.util.List;

import com.pnv.models.Product;

public interface SearchDAO {
	public List<Product> getProductsInRangeOfPrice(int price1, int price2);
	public List<Product> showProductsWithSearchCondition(String conditionSearch);
}

package cop;

import java.util.List;
import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pnv.models.Product;

@Service
@Transactional
public class SearchDAOImpl implements SearchDAO{
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public List<Product> getProductsInRangeOfPrice(int price1, int price2) {
		List<Product> products;
		Query query = (Query) sessionFactory.getCurrentSession().createQuery("from Product AS P WHERE P.product_price BETWEEN " + price1 + " AND " + price2);
		
		products = (List<Product>) query.list();
		return products;
	}

	public List<Product> showProductsWithSearchCondition(String conditionSearch) {
		List<Product> products;
		Query query = (Query) sessionFactory.getCurrentSession().createQuery("from Product AS P WHERE P.product_name LIKE '%"+ conditionSearch +"%'");
		
		products = (List<Product>) query.list();
		return products;
	}

}

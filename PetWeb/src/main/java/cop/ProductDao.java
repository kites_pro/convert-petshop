package cop;

import java.util.List;

import com.pnv.models.Product;

public interface ProductDao {
	public List<Product> getAllProduct();
	void saveOrUpdate(Product product);
}

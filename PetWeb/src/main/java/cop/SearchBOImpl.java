package cop;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pnv.dao.SearchDAO;
import com.pnv.models.Product;

@Service
@Transactional
public class SearchBOImpl implements SearchBO {
	
	@Autowired
	private SearchDAO searchDAO;
	
	public List<Product> showProductInRangeOfPrice(int price1, int price2) {
		return this.searchDAO.getProductsInRangeOfPrice(price1, price2);
		
	}

	public List<Product> showProductsWithSearchCondition(String conditionSearch) {
		return this.searchDAO.showProductsWithSearchCondition(conditionSearch);
	}

}

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.pnv.model.Product"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>content</title>
</head>
<body>
	<%
		List<Product> listProduct = (List<Product>) request.getAttribute("product");
		int total = (int) request.getAttribute("total");
	%>
	<!---->
	<div class="container">
		<div class="content">
			<div class="content-top">
				<h3 class="future">FEATURED</h3>
				<div class="content-top-in">

					<%
						for (Product p : listProduct) {
					%>

					<div class="col-md-3 md-col">
						<div class="col-md">
							<a href="single?productID=<%=p.getProductID()%>"> <img
								src="<%=p.getProductImage()%>" alt="<%=p.getProductName()%>"
								width="239px" height="207px" /></a>
							<div class="top-content">
								<h5>
									<a href="single?productID=<%=p.getProductID()%>"><%=p.getProductName()%></a>
								</h5>
								<div class="white">
									<a
										href="CartServlet?command=plus&productID=<%=p.getProductID()%>"
										class="hvr-shutter-in-vertical hvr-shutter-in-vertical2 ">ADD
										TO CART</a>
									<p class="dollar">
										<span class="in-dollar">$</span><span><%=p.getPrice()%></span>
									</p>
									<div class="clearfix"></div>
								</div>

							</div>
						</div>
					</div>
					<%
						}
					%>
					<div class="clearfix"></div>
				</div>
			</div>

		</div>
	</div>

</body>
</html>

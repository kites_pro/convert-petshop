<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>register</title>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"
	type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function() {
		var x_timer;
		$("#email").keyup(function(e) {
			clearTimeout(x_timer);
			var user_name = $(this).val();
			x_timer = setTimeout(function() {
				check_username_ajax(user_name);
			}, 1000);
		});

		function check_username_ajax(username) {
			$("#user-result").html('<img src="img/ajax-loader.gif" />');
			$.post('CheckEmailServlet', {
				'username' : username
			}, function(data) {
				$("#user-result").html(data);
			});
		}
	});
</script>
</head>
<body>

	<jsp:include page="header.jsp"></jsp:include>

	<div class="container">
		<div class="account">
			<h2 class="account-in">Register</h2>
			<form action="users" method="POST">
				<div>
					
					
					<label for="email">User Name<span class="required">*</span></label>
					
					<input type="email"
						name="email" id="email" pattern="[a-zA-Z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*" required placeholder="Enter you Email (xxx@xxx.xxx)"> <span id="user-result"></span>
				</div>
				<div>
					<label for="pass">Password <span class="required">*</span></label>
					
					 <input type="password"
						name="pass" pattern=".{3,8}" required placeholder="Enter you Password (3-8 kí tự)"> <span></span>
				</div>
				<p class="requiredd">* Yêu cầu bắt buộc</p>
				<input type="hidden" value="insert" name="command"> <input
					type="submit" value="Register">
			</form>
		</div>
	</div>


	<jsp:include page="footer.jsp"></jsp:include>

</body>
</html>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.pnv.dao.SearchImpl"%>
<%@page import="com.pnv.model.Product"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Search</title>  
</head>
<body>
			<%
				ArrayList<Product> listProduct= (ArrayList<Product>) request.getAttribute("products");
			%>
<jsp:include page="header.jsp"></jsp:include>

	<br />
	<br />
	<!---->
	<div class="container">
		<div class="content">
			<div class="content-top">
				<h3 class="future">FEATURED</h3>
				<div class="content-top-in">

					<%
						for (Product p : listProduct) {
					%>

					<div class="col-md-3 md-col">
						<div class="col-md">
							<a href="single?productID=<%=p.getProductID()%>"><img
								src="<%=p.getProductImage()%>" alt="<%=p.getProductName()%>" /></a>
							<div class="top-content">
								<h5>
									<a href="single?productID=<%=p.getProductID()%>"><%=p.getProductName()%></a>
								</h5>
								<div class="white">
									<a
										href="CartServlet?command=plus&productID=<%=p.getProductID()%>"
										class="hvr-shutter-in-vertical hvr-shutter-in-vertical2 ">ADD
										TO CART</a>
									<p class="dollar">
										<span class="in-dollar">$</span><span><%=p.getPrice()%></span>
									</p>
									<div class="clearfix"></div>
								</div>

							</div>
						</div>
					</div>

					<%
						}
					%>


					<div class="clearfix"></div>
				</div>
			</div>
			<!---->


			<%-- <ul class="start">
				<li><a href="#"><i></i></a></li>
				<%
					for (int i = 1; i <= (total / 8) + 1; i++) {
				%>
				<li class="arrow"><a
					href="product?categoryID=<%=categoryID%>&pages=<%=i%>"><%=i%></a></li>
				<%
					}
				%>
				<li><a href="#"><i class="next"> </i></a></li>
			</ul> --%>
		</div>
	</div>

    <jsp:include page="footer.jsp"></jsp:include>
	
</body>


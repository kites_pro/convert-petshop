<%@page import="com.pnv.model.Item"%>
<%@page import="java.util.Map"%>
<%@page import="com.pnv.model.Cart"%>
<%@page import="com.pnv.model.Users"%>
<%@page import="com.pnv.model.Category"%>
<%@page import="com.pnv.dao.CategoryDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>header</title>

<div id="fb-root"></div>
<script>
	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id))
			return;
		js = d.createElement(s);
		js.id = id;
		js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.6&appId=381324158709242";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
</script>

</head>
<body>

	<%
		CategoryDAO categoryDAO = new CategoryDAO();
		Users users = null;
		if (session.getAttribute("user") != null) {
			users = (Users) session.getAttribute("user");
		}
		Cart cart = (Cart) session.getAttribute("cart");
		if (cart == null) {
			cart = new Cart();
			session.setAttribute("cart", cart);
		}
	%>
	

	<!--header-->
	<div class="header">
		<div class="header-top">
			<div class="container">
				<div class="header-top-in">
					<div class="logo">
						<a href="${pageContext.request.contextPath}/"><img
							src="images/logo.png" alt=" "></a>
					</div>
					<div class="header-in">
						<ul class="icon1 sub-icon1">
							<%
								if (users != null) {
							%>
							<li><a href="#"><%=users.getUserEmail()%></a></li>
							<%
								}
							%>

							<!--   <li><a href="wishlist.jsp">WISH LIST (0)</a> </li>
                            <li><a href="account.jsp">  MY ACCOUNT</a></li>
                            <li><a href="#"> SHOPPING CART</a></li> -->



							<li><a href="checkOut">CHECKOUT</a></li>
							<li><div class="cart">
									<a href="#" class="cart-in"> </a> <span> <%=cart.countItem()%></span>
								</div>
								<ul class="sub-icon1 list">
									<h3>Recently added items</h3>
									<div class="shopping_cart">

										<%
											for (Map.Entry<Long, Item> list : cart.getCartItems().entrySet()) {
										%>
										<div class="cart_box">
											<div class="message">
												<div class="alert-close">
													<a href="CartServlet?command=remove&productID=<%=list.getValue().getProduct().getProductID()%>"></a>
												</div>
												<div class="list_img">
													<img
														src="<%=list.getValue().getProduct().getProductImage()%>"
														class="img-responsive" alt="">
												</div>
												<div class="list_desc">
													<h4>
														<%=list.getValue().getProduct().getProductName()%>
													</h4>
													<%=list.getValue().getQuantity()%>
													x<span class="actual"> $<%=list.getValue().getProduct().getPrice()%></span>
												</div>
												<div class="clearfix"></div>
											</div>
										</div>
										<%
											}
										%>


									</div>
									<div class="total">
										<div class="total_left">Cart Subtotal:</div>
										<div class="total_right">
											$<%=cart.totalCart()%></div>
										<div class="clearfix"></div>
									</div>
									<div class="login_buttons">
										<div class="check_button">
											<a href="checkout">Check out</a>
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="clearfix"></div>
								</ul></li>
						</ul>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>

		<div class="header-bottom">
			<div class="container">
				<div class="h_menu4">
					<!-- <a class="toggleMenu" href="#">Menu</a> -->
					<ul class="nav">
						<li class="active"><a
							href="${pageContext.request.contextPath}/"><i> </i>Home</a></li>
						<li>Danh mục
							<ul class="drop">
								<%
									for (Category c : categoryDAO.getListCategory()) {
								%>
								<li>
									<a
										href="product?categoryID=<%=c.getCategoryID()%>&pages=1"><%=c.getCategoryName()%>
									</a>
								</li>
								<%
									}
								%>
							</ul></li>

						<li><a href="contact">Contact </a></li>

					</ul>
					<script type="text/javascript" src="js/nav.js"></script>
				</div>
			</div>
		</div>
		<div class="header-bottom-in">
			<div class="container">
				<div class="header-bottom-on">
					<p class="wel">
						Welcome visitor you can<a href="login"> login</a> or <a
							href="register"> create an account.</a>
					</p>
					<div class="header-can">

						<!--     <ul class="social-in">
                            <li><a href="#"><i> </i></a></li>
                            <li><a href="#"><i class="facebook"> </i></a></li>
                            <li><a href="#"><i class="twitter"> </i></a></li>					
                            <li><a href="#"><i class="skype"> </i></a></li>
                        </ul>	
                        
                        -->
						<div class="down-top">
							<form action="side" method="GET">
								<select class="in-drop" id="Price" name="Price" onchange="this.form.submit()">
									<option class="in-of">--Select Price--</option>
									<option value="Under 100.000" class="in-of">Under 100.000</option>
									<option value="100.000-300.000" class="in-of">100.000-300.000</option>
									<option value="300.000-500.000" class="in-of">300.000-500.000</option>
									<option value="More 500.000" class="in-of">More 500.000</option>
								</select>
							</form>
						</div>
						<div class="search">
							<form action="side" method="POST">
								<input type="text" name="Search"
									placeholder="Enter search name..."> <input
									type="submit" value="">
							</form>
						</div>

						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>

</body>
</html>
